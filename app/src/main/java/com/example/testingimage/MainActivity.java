package com.example.testingimage;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    HTTPHandler handler;

    RecyclerView list;

    GridLayoutManager layout_manager;
    View.OnClickListener onImage;
    Adapter adapter;
    JSONArray array;
    JSONObject object;
    String web_result;
    View.OnClickListener onClick;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);

        adapter = new Adapter();
        handler = new HTTPHandler();
        adapter.activity = this;
        layout_manager = new GridLayoutManager(this, 1);

        list.setLayoutManager(layout_manager);


        //  Log.e("test", "http://192.168.0.105:3008/get_images");
        //  web_result = handler.makeRequest("http://192.168.0.105:3008/get_images=");
        //web_result = "[{image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}, {image: 'https://skazkakafe.ru/wp-content/uploads/2023/02/минибургер-4.jpg'}, {image: 'https://i.pinimg.com/736x/eb/cc/79/ebcc79e2985d8a679f56f30aa8a568c1.jpg'}, {image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}]";

        web_result = "[{ image: 'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg'}," +
                " { image: 'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg'}, " +
                "{ image: 'https://i.pinimg.com/736x/eb/cc/79/ebcc79e2985d8a679f56f30aa8a568c1.jpg'}," +
                " { image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://1.bp.blogspot.com/-N8K-ofVV96E/XYxTMnVWXqI/AAAAAAAAATk/QxeBWGQR8VQW8_XfnCkeEo_DNvPLKncbACLcBGAsYHQ/s640/ad_logo_twitter_card.png'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}," +
                "{ image: 'https://tri-gusya.ru/wp-content/uploads/2021/11/salat-czezar-405x330.jpg'}]";
        //  web_result="[{id:1,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'один'},{id:2,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'два'},{id:3,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'три'},{id:4,image:'https://img.goodfon.com/original/1400x1050/b/d2/martin-kuhn-anna-martin-kuhn-model-girl-brunette-long-hair-s.jpg',title:'четыри}]";
        try {
            array = new JSONArray(web_result);
            list.setAdapter(adapter);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }
    public  void onClick(String text){
        Toast.makeText(this, text,Toast.LENGTH_LONG).show();

    }

}
