package com.example.testingimage;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;
import android.widget.Toast;
import android.view.Gravity;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    MainActivity activity;
    JSONObject object;
    View.OnClickListener onClick1;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_image, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;



    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, price;
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.image);
            //   this.title = itemView.findViewById(R.id.title);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        onClick1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onClick("Как то так получилось");



            }
        };
        viewHolder.image.setOnClickListener(onClick1);

        try {

            object = activity.array.getJSONObject(position);
            //  viewHolder.title.setText(object.getString("title"));
            Glide.with(activity).load(object.getString("image")).into(viewHolder.image);
            activity.onClick("как то так получилось");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public int getItemCount() {

        return activity.array.length();
    }
}
